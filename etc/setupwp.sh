if [ "$EUID" -ne 0 ];  then
  echo "Please run as root"
  exit
fi

if [ ! -e /home/vagrant/www/wp]; then
  echo "path '/home/vagrant/www/wp' does not exist"
  exit
fi

if ! grep -q "dev.fei.local" /etc/hosts; then
  echo "append to host file"
  echo "" >> /etc/hosts
  echo "127.0.0.1 wp.local" >> /etc/hosts
fi

if [ ! -f /etc/apache2/ssl/server.crt ]; then
  echo "Installing SSL ##########################################################"
  mkdir /etc/apache2/ssl
  openssl genrsa -des3 -passout pass:x -out /etc/apache2/ssl/server.pass.key 2048
  openssl rsa -passin pass:x -in /etc/apache2/ssl/server.pass.key -out /etc/apache2/ssl/server.key
  rm /etc/apache2/ssl/server.pass.key
  openssl req -new -key /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.csr -subj "/C=US/ST=DC/L=Texas/O=Todd/OU=IT Department/CN=*.fei.com"
  openssl x509 -req -days 365 -in /etc/apache2/ssl/server.csr -signkey /etc/apache2/ssl/server.key -out /etc/apache2/ssl/server.crt

fi

cp /vagrant/etc/wp.conf /etc/apache2/sites-available/wp.conf
ln -s /etc/apache2/sites-available/wp.conf /etc/apache2/sites-enabled/wp.conf

sudo service apache2 restart


if [[ ! -z "`mysql -uroot -ppassword -qfsBe "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='wp'" 2>&1`" ]]; then
  echo "DATABASE ALREADY EXISTS"
else
  mysql -uroot -ppassword < /vagrant/etc/createwp.sql
fi
