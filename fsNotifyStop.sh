#!/bin/bash

echo "halting vagrant fsnotify";
if [ -f /tmp/vagrant_fsnotify.log ]; then
  rm /tmp/vagrant_fsnotify.log
fi;

ps -ef | grep 'vagrant fsnotify' | grep -v grep | awk '{print $2}' | xargs -r kill -9
