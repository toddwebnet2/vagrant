class Todd
  def Todd.configure(config, settings, base_config)
    scripts_dir = File.join(settings["vagrant_dir"], 'features')

    settings['features'] = (settings['features'] || []) | []
    # settings['ports'] = (settings['ports'] || []) | [
    #     { "send" => 80, "to" => 80 }, # HTTP
    #     { "send" => 3360, "to" => 3360 }, # MySQL
    #     { "send" => 5432, "to" => 5432 }, # PostgreSQL
    # ]

    if settings.include? 'name'
      config.vm.hostname = settings["hostname"] ||= settings["name"] + "-vagrant"
    else
      config.vm.hostname = settings["hostname"] ||= "vagrant"
    end

    config.vm.box = settings["box"] ||= "ubuntu/xenial64"

    if Vagrant.has_plugin?('vagrant-hostsupdater')
      hosts = []
      if settings.include? 'sites'
        settings["sites"].each do |site|
          hosts.push(site)
        end
      end

      # Pass the found host names to the hostsupdater plugin so it can perform magic.
      config.hostsupdater.aliases = hosts
      config.hostsupdater.remove_on_suspend = true
    end


    if 'public'.eql?(settings["network"])
        if settings.include? 'ip'
            config.vm.network "public_network", ip: settings["ip"]
        else
            config.vm.network "public_network"
        end
    else
        config.vm.network :private_network, id: "primary", ip: settings["ip"] ||= "192.168.50.4"
    end

    config.vm.provider :virtualbox do |v|
      v.gui = true.eql?(settings['debug']) # show console output
      v.customize ["modifyvm", :id, "--memory", settings["memory"] ||= "2048"]
      v.customize ["modifyvm", :id, "--cpus", settings["cpus"] ||= "1"]
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]

      # Set the box name in VirtualBox to match the working directory.
      pwd = Dir.pwd
      v.name = settings["name"] ||= File.basename(pwd)
    end

    # Register All Of The Configured Shared Folders
    if settings.include? 'folders'
      m_defaults = ["dmode=777", "fmode=777"]
      settings["folders"].each do |folder|
        mount_opts = []

        if (folder["type"] == "nfs")
          mount_opts = folder["mount_options"] ? folder["mount_options"] : ['actimeo=1']
        elsif (folder["type"] == "smb")
          mount_opts = folder["mount_options"] ? folder["mount_options"] : ['vers=3.02', 'mfsymlinks']
        end

        # For b/w compatibility keep separate 'mount_opts', but merge with options.
        # Set shared folder owner:group and permissions for docker containers
        options = (folder["options"] || {}).merge({
            owner: 'vagrant', group: 'vagrant', mount_options: mount_opts | m_defaults
        })

        # Double-splat (**) operator only works with symbol keys, so convert
        options.keys.each{|k| options[k.to_sym] = options.delete(k) }

        config.vm.synced_folder folder["map"], folder["to"], type: folder["type"] ||= nil, fsnotify: true, **options
      end
    end

    # Forward ports
    if settings.include? 'ports'
      settings["ports"].each do |port|
        config.vm.network "forwarded_port", guest: port["to"], host: port["send"], auto_correct: true
      end
    end
    config.vm.network "forwarded_port", guest: 3306, host: 3306

    config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/me.pub"
    config.vm.provision "shell", path: File.join(scripts_dir, 'base_provision.sh')

    # Provision features
    if settings.include? 'features'
      settings["features"].each do |feature|
        if File.exists?(File.join(scripts_dir, feature, 'provision.sh')) then
          config.vm.provision "shell", path: File.join(scripts_dir, feature, 'provision.sh')
        end
        if feature == 'fsnotify'
          base_config.trigger.after :up do |t|
            t.name = "vagrant-fsnotify"
            if RUBY_PLATFORM.downcase =~ /linux/ || RUBY_PLATFORM.downcase =~ /mac/
                t.run = { inline: "bash fsNotifyStart.sh" }
            end
          end

          base_config.trigger.after :halt do |t|
            if RUBY_PLATFORM.downcase =~ /linux/ || RUBY_PLATFORM.downcase =~ /mac/
              t.run = { inline: "bash fsNotifyStop.sh" }
            end
          end

        end
      end
    end



  end
end
