if [ ! -f /etc/apache2/apache2.conf ]; then
  echo "Installing Apache ##########################################################"
  apt-get update
  apt-get install apache2 curl apache2-utils  -y
  a2enmod rewrite
  a2enmod ssl
  echo "ServerName localhost" >> /etc/apache2/apache2.conf

  service apache2 restart
else
  echo "Apache already installed ##########################################################"
fi
