if [ ! -f /etc/php/7.1/apache2/php.ini ]; then
  echo "Installing PHP 7.1"
  add-apt-repository ppa:ondrej/php -y > /dev/null
  apt-get update > /dev/null
  apt-get install zip unzip php7.1-zip php7.1 libapache2-mod-php7.1 php7.1-mbstring php7.1-cli php7.1-pgsql php7.1-bcmath -y > /dev/null  
  apt-get install php7.1-gd php7.1-curl php7.1-dom  php7.1-soap -y > /dev/null

fi
