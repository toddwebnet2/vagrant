
if [ ! -d /etc/supervisor/conf.d ]; then
  echo "Installing Supervisor ##########################################################"
  apt-get update
  sudo apt-get install supervisor -y
else
  echo "Supervisor already installed ##########################################################"
fi
