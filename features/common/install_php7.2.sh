if [ ! -f /etc/php/7.2/apache2/php.ini ]; then
  echo "Installing PHP 7.2"
  add-apt-repository ppa:ondrej/php -y > /dev/null
  apt-get update > /dev/null
  apt-get install zip unzip php7.2-zip php7.2 libapache2-mod-php7.2 php7.2-mbstring php7.2-cli php7.2-pgsql php7.2-bcmath -y > /dev/null
  apt-get install php7.2-gd php7.2-curl php7.2-dom  php7.2-soap -y > /dev/null

fi
