if [ ! -f /etc/php/5.6/apache2/php.ini ]; then
   echo "Installing PHP 5.6"
  add-apt-repository ppa:ondrej/php -y > /dev/null
  apt-get update > /dev/null
  apt-get install zip unzip php5.6-zip php5.6 libapache2-mod-php5.6 php5.6-mbstring php5.6-cli php5.6-pgsql php5.6-bcmath -y > /dev/null
  ln -s /etc/php/5.6/apache2/php.ini /etc/php.ini
  apt-get install php5.6-gd php5.6-curl php5.6-dom php5.6-soap -y > /dev/null
fi
