if [ ! -f /usr/local/bin/composer ]; then
  echo "Installing Composer"
  curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
fi
