
if [ ! -f /etc/postgresql/9.5/main/postgresql.conf ]; then
  echo "Installing postgresql"
  echo -e "# PostgreSQL repository \ndeb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - > /dev/null
  apt-get update -y > /dev/null
  apt-get install postgresql-9.5 -y > /dev/null
  apt-get install postgresql-plperl-9.5 -y > /dev/null
  echo "" >> /etc/postgresql/9.5/main/postgresql.conf
  echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/9.5/main/pg_hba.conf
  echo "" >> /etc/postgresql/9.5/main/postgresql.conf
  echo "listen_addresses = '*'" >> /etc/postgresql/9.5/main/postgresql.conf
  sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'abc123';"
  service postgresql restart
fi
