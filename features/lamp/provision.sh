#! /usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Install base packages ---\n"
apt-get -y install vim curl build-essential python-software-properties git > /dev/null

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update


  echo "Installing Apache ##########################################################"
  apt-get update
  apt-get install apache2 curl apache2-utils  -y
  a2enmod rewrite
  a2enmod ssl
  echo "ServerName localhost" >> /etc/apache2/apache2.conf
  service apache2 restart


  sudo apt-get -y install software-properties-common

  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password password'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password password'
  sudo apt-get -y install mysql-server


echo -e "\n--- Installing PHP-specific packages ---\n"
echo "Installing PHP 7.2"
add-apt-repository ppa:ondrej/php -y > /dev/null
apt-get update -y > /dev/null
apt-get install -y zip unzip php7.2-zip php7.2 libapache2-mod-php7.2 php7.2-mbstring php7.2-cli php7.2-pgsql php7.2-bcmath php7.2-gd php7.2-curl php7.2-dom  php7.2-soap php7.2-curl php7.2-mysql php7.2-mysqli php7.2-gettext gcc make autoconf libc-dev pkg-config libmcrypt-dev




#echo -e "\n--- Setting up our MySQL user and db ---\n"
#mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME" > /dev/null
#mysql -uroot -p$DBPASSWD -e "grant all privileges on $DBNAME.* to '$DBUSER'@'localhost' identified by '$DBPASSWD'" > /vagrant/vm_build.log 2>&1
#mysql -uroot -p -e "grant all privileges on fulfillment.* to 'fulfillment'@'localhost' identified by 'abc123'"

#apt-get -y install php apache2 libapache2-mod-php php-curl php-gd php-mysql php-gettext > /dev/null

echo -e "\n--- Enabling mod-rewrite ---\n"
a2enmod rewrite > /dev/null

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- We definitly need to see the PHP errors, turning them on ---\n"
# sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php/7.1/apache2/php.ini
# sed -i "s/display_errors = .*/display_errors = On/" /etc/php/7.1/apache2/php.ini
apt-get install -y php7.2-mysqli > /dev/null
echo -e "\n--- Restarting Apache ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "\n--- Installing Composer for PHP package management ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null
mv composer.phar /usr/local/bin/composer


sudo apt-get autoremove -y > /dev/null
