
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) main"
curl -fsSL "https://download.docker.com/linux/ubuntu/ubuntu/gpg" | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install docker-ce --allow-unauthenticated

sudo systemctl enable docker
#  sudo systemctl disable docker
