#! /usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive

sudo apt-get update > /dev/null
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common > /dev/null
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > /dev/null

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
# sudo apt-key adv -k 58118E89F3A912897C070ADBF76221572C52609D

sudo apt-get update > /dev/null
sudo apt-get -y install docker-ce --allow-unauthenticated > /dev/null

sudo systemctl enable docker > /dev/null

sudo usermod -a -G docker vagrant

#  sudo systemctl disable docker
