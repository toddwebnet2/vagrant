#! /usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive


sudo apt-get update
sudo apt-get install -y python-software-properties
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get -y install nodejs

sudo npm install -g @angular/cli
