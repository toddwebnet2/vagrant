export DEBIAN_FRONTEND=noninteractive

sudo apt-get update > /dev/null
sudo apt-get install -y php-pear php7.0-dev > /dev/null

curl -s https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo bash -c "curl -s https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list" > /dev/null
sudo apt-get update > /dev/null
sudo ACCEPT_EULA=Y apt-get -y install msodbcsql mssql-tools > /dev/null
sudo apt-get -y install unixodbc-dev > /dev/null

sudo apt-get -y install gcc g++ make autoconf libc-dev pkg-config > /dev/null
sudo pecl install sqlsrv > /dev/null
sudo pecl install pdo_sqlsrv > /dev/null

# sudo bash -c "echo extension=sqlsrv.so > /etc/php7.0/conf.d/sqlsrv.ini"
# sudo bash -c "echo extension=pdo_sqlsrv.so > /etc/php7.0/conf.d/pdo_sqlsrv.ini"
# /etc/php/7.0/apache2/php.ini
# /etc/php/7.0/cli/php.ini
# extension=sqlsrv.so
# extension=pdo_sqlsrv.so

rm -f /etc/php/7.0/mods-available/pdo_sqlsrv.ini
touch /etc/php/7.0/mods-available/pdo_sqlsrv.ini
echo "" >> /etc/php/7.0/mods-available/pdo_sqlsrv.ini
echo "extension=pdo_sqlsrv.so" >> /etc/php/7.0/mods-available/pdo_sqlsrv.ini
rm -f /etc/php/7.0/apache2/conf.d/20-pdo_sqlsrv.ini
rm -f /etc/php/7.0/cli/conf.d/20-pdo_sqlsrv.ini
ln -s /etc/php/7.0/mods-available/pdo_sqlsrv.ini /etc/php/7.0/apache2/conf.d/20-pdo_sqlsrv.ini
ln -s /etc/php/7.0/mods-available/pdo_sqlsrv.ini /etc/php/7.0/cli/conf.d/20-pdo_sqlsrv.ini


rm -f /etc/php/7.0/mods-available/sqlsrv.ini
touch /etc/php/7.0/mods-available/sqlsrv.ini
echo "" >> /etc/php/7.0/mods-available/sqlsrv.ini
echo "extension=sqlsrv.so" >> /etc/php/7.0/mods-available/sqlsrv.ini
rm -f /etc/php/7.0/cli/conf.d/20-sqlsrv.ini
rm -f /etc/php/7.0/apache2/conf.d/20-sqlsrv.ini
ln -s /etc/php/7.0/mods-available/sqlsrv.ini /etc/php/7.0/apache2/conf.d/20-sqlsrv.ini
ln -s /etc/php/7.0/mods-available/sqlsrv.ini /etc/php/7.0/cli/conf.d/20-sqlsrv.ini
