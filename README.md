Required:
vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-winnfsd
vagrant plugin install vagrant-hostsupdater


Vagrant Commands:

vagrant up
vagrant halt
vagrant provision
vagrant up --provision
vagrant reload
vagrant destroy
